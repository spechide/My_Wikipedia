module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            main: {
                files: [{
                    src: ['Gruntfile.js']
                }]
            },
            options: {
                globals: {
                    'jQuery': true,
                    'angular': true,
                    'console': true,
                    '$': true,
                    '_': true,
                    'moment': true
                }
            }
        },

        htmlhint: {
            options: {
                'attr-lower-case': true,
                'attr-value-not-empty': false,
                'tag-pair': true,
                'tag-self-close': true,
                'tagname-lowercase': true,
                'id-class-value': true,
                'id-unique': true,
                'img-alt-require': true,
                'img-alt-not-empty': true
            },
            main: {
                src: ['public/**.html']
            }
        },

        concat: {
          app: {
              src: ['index.html'],
              dest: 'public/index.html'
          }
        },

        watch: {
            grunt: {
                files: ['Gruntfile.js'],
                options: {
                    nospawn: true,
                    keepalive: true,
                    livereload: true
                },
                tasks: ['public:dev']
            },
            html: {
                files: ['**.html'],
                options: {
                    nospawn: true,
                    keepalive: true,
                    livereload: true
                },
                tasks: ['htmlhint']
            },
            js: {
                files: '**',
                options: {
                    nospawn: true,
                    livereload: true
                },
                tasks: ['jshint', 'concat']
            },
           css: {
               files: '**',
               options: {
                   nospawn: true,
                   livereload: true
               },
               tasks: ['concat']
           }
        },

        connect: {
            server: {
                options: {
                    port: 8080,
                    base: 'public',
                    hostname: 'localhost',
                    livereload: true
                }
            }
        },

        uglify: {
          compress: {
            files: {
              'public/index.js': [
                'index.js'
              ]
            },
            options: {
              mangle: false,
              unused: true
            }
          }
        },

        cssmin: {
          options: {
            sourceMap: false
          },
          compress: {
            files: {
              'public/index.css': [
                'public/bower_components/bootstrap/dist/css/bootstrap.min.css',
                'public/bower_components/normalize-css/normalize.css',
                'index.css'
              ]
            }
          }
        }
    });

    grunt.registerTask('public:dev', function(target) {
        grunt.task.run([
            'concat',
            'uglify',
            'cssmin',
            'htmlhint',
            'jshint'
        ]);
    });

    grunt.registerTask('serve', function(target) {
        grunt.task.run([
            'public:dev',
            'connect',
            'watch'
        ]);
    });

};
